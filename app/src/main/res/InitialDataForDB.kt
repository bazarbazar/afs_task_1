data class InitialDataForDB(
    val id: Int,
    val name: String,
    val status: String
)
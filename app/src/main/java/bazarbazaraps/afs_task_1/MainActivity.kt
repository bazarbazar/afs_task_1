package bazarbazaraps.afs_task_1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream


class MainActivity : AppCompatActivity() {

    var arr = arrayListOf<AFS_Task>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dbhandler = MyDBHandler(this, null, null, 1)

//----------------BUTTONS-------------------
        val buttonStartWork = findViewById<Button>(R.id.button_start_work)
        val buttonStop = findViewById<Button>(R.id.button_stop)
        val buttonStartTravel = findViewById<Button>(R.id.button_start_traveling)

        var selectedID : Long
        var taskStatus : String
        var allRowsAmount : Int
        var openStatusAmount :Int

//----------------HEAD LIST-------------------
        val AFSTasksHEAD1 = AFS_Task_HEAD(dbhandler.getColumnNames()[0], dbhandler.getColumnNames()[1], dbhandler.getColumnNames()[2])
        AFS_Tasks_HEAD_ListSingleton.instance.AFS_Task_HEAD_table = arrayListOf<AFS_Task_HEAD>(
            AFSTasksHEAD1
        )
        val AFSTasksHEADlistView = findViewById<ListView>(R.id.ASF_Tasks_HEAD_ListView)
        val adapterHead = AFS_Tasks_HEAD_ListAdapter(this, AFS_Tasks_HEAD_ListSingleton.instance.AFS_Task_HEAD_table)
        AFSTasksHEADlistView.adapter=adapterHead

//-----------------LIST---------------------
        val listView: ListView
        val adapter:AFS_Tasks_CursorAdapter

        listView = findViewById<ListView>(R.id.AFS_Tasks_ListView)
        adapter = AFS_Tasks_CursorAdapter(this, dbhandler.getAFSTasksListContents())
        listView.adapter = adapter
        listView.setChoiceMode(1)

        fun initData() {
            if (dbhandler.getAFSTasksRowsCount() == 0) {

                arr.forEach{
                    dbhandler.insertAFSTask(AFS_Task(it._id,it.name,it.status) )}
                adapter.changeCursor(dbhandler.getAFSTasksListContents())

                /*
                val AFSTask1 = AFS_Task(1, "Laser Canon", "Open")
                val AFSTask2 = AFS_Task(2, "Light Chaser", "Open")
                val AFSTask3 = AFS_Task(3, "Wolverun 13", "Open")
                val AFSTask4 = AFS_Task(4, "Alien Destroyer", "Open")
                val AFSTask5 = AFS_Task(5, "Goat Ship", "Open")
                val AFSTask6 = AFS_Task(6, "Heavy Warship", "Open")
                val AFSTask7 = AFS_Task(7, "Plasma Chaser", "Open")
                val AFSTask8 = AFS_Task(8, "White Star", "Open")
                val AFSTask9 = AFS_Task(9, "Laser Canon 1", "Open")
                val AFSTask10 = AFS_Task(10, "Light Chaser 1", "Open")
                val AFSTask11 = AFS_Task(11, "Wolverun 13 1", "Open")
                val AFSTask12 = AFS_Task(12, "Alien Destroyer 1", "Open")
                val AFSTask13 = AFS_Task(13, "Goat Ship 1", "Open")
                val AFSTask14 = AFS_Task(14, "Heavy Warship 1", "Open")
                val AFSTask15 = AFS_Task(15, "Plasma Chaser 1", "Open")
                val AFSTask16 = AFS_Task(16, "White Star 1", "Open")

                dbhandler.insertAFS_Task(AFS_Task_1)
                dbhandler.insertAFS_Task(AFS_Task_2)
                dbhandler.insertAFS_Task(AFS_Task_3)
                dbhandler.insertAFS_Task(AFS_Task_4)
                dbhandler.insertAFS_Task(AFS_Task_5)
                dbhandler.insertAFS_Task(AFS_Task_6)
                dbhandler.insertAFS_Task(AFS_Task_7)
                dbhandler.insertAFS_Task(AFS_Task_8)
                dbhandler.insertAFS_Task(AFS_Task_9)
                dbhandler.insertAFS_Task(AFS_Task_10)
                dbhandler.insertAFS_Task(AFS_Task_11)
                dbhandler.insertAFS_Task(AFS_Task_12)
                dbhandler.insertAFS_Task(AFS_Task_13)
                dbhandler.insertAFS_Task(AFS_Task_14)
                dbhandler.insertAFS_Task(AFS_Task_15)
                dbhandler.insertAFS_Task(AFS_Task_16)
                */

            }
        }

        fun readJson() {
            val json: String?

            try {
                val inputStream: InputStream = assets.open("initial_data_for_DB.json")
                json = inputStream.bufferedReader().use{it.readText()}

                val jsonarr = JSONArray(json)
                for (i in 0..jsonarr.length()-1){
                    val jsonobj = jsonarr.getJSONObject(i)

                    arr.add( AFS_Task(jsonobj.getString("id").toInt(),jsonobj.getString("name"),jsonobj.getString("status")) )

                }
            }
            catch (e : IOException) {
            }
        }

        fun buttonVisibility(){

            if (listView.checkedItemPosition == -1){
                buttonStartTravel.isEnabled = false
                buttonStartWork.isEnabled = false
                buttonStop.isEnabled = false
            }else{

                //var selected_ID = dbhandler.getAFS_Tasks_ID(listView.adapter.getItem(listView.checkedItemPosition) as Cursor)
                selectedID = listView.adapter.getItemId(listView.checkedItemPosition)

                taskStatus = dbhandler.getAFSTasksStatus(selectedID.toInt())
                allRowsAmount = dbhandler.getAFSTasksRowsCount()
                openStatusAmount = dbhandler.getAFSTasksStatusCount("Open")

                if ((taskStatus == "Open") && (allRowsAmount == openStatusAmount)){
                    buttonStartTravel.isEnabled=true
                    buttonStartWork.isEnabled=false
                    buttonStop.isEnabled=false

                }
                else if  (taskStatus=="Traveling"){
                    buttonStartWork.isEnabled=true
                    buttonStartTravel.isEnabled=false
                    buttonStop.isEnabled=false
                }
                else if (taskStatus=="Working"){
                    buttonStop.isEnabled=true
                    buttonStartTravel.isEnabled=false
                    buttonStartWork.isEnabled=false
                }else{
                    buttonStartTravel.isEnabled=false
                    buttonStartWork.isEnabled=false
                    buttonStop.isEnabled=false
                }
            }
        }


        listView.setOnItemClickListener { parent, view, position, id ->

            selectedID = listView.adapter.getItemId(listView.checkedItemPosition)

            Toast.makeText(
                this.baseContext, "Task ID = " + selectedID.toString() + " selected",
                Toast.LENGTH_LONG
            ).show()

            System.out.println(selectedID);
            buttonVisibility()
        }


        buttonStartTravel.setOnClickListener{buttonView->
            /*
                        Toast.makeText(
                            this.baseContext, (AFS_Tasks_ListSingleton.instance.AFS_Task_table[listView.checkedItemPosition]._id).toString(),
                            Toast.LENGTH_LONG).show()
                            */

            if(listView.checkedItemPosition!=-1) {

                // IF Selected ID task status==OPEN and open_count==all_rows_count
                //change in DB for ID= X status
                //update list from DB and refresh View and refresh buttons

                selectedID = listView.adapter.getItemId(listView.checkedItemPosition)
                taskStatus = dbhandler.getAFSTasksStatus(selectedID.toInt())
                allRowsAmount = dbhandler.getAFSTasksRowsCount()
                openStatusAmount = dbhandler.getAFSTasksStatusCount("Open")

                if ((taskStatus == "Open") && (allRowsAmount == openStatusAmount)) {
                    dbhandler.updateAFSTaskStatus("Traveling", selectedID.toInt())
                    adapter.changeCursor(dbhandler.getAFSTasksListContents())
                    Toast.makeText(
                        this.baseContext, "Item with ID = "+selectedID.toString()+ " has changed status to 'Traveling'",
                        Toast.LENGTH_LONG
                    ).show()

                } else {
                    Toast.makeText(
                        this.baseContext, "Unable to set status 'Traveling'",
                        Toast.LENGTH_LONG
                    ).show()
                }

            }else {
                Toast.makeText(this.baseContext, "Select ITEM no the list", Toast.LENGTH_LONG)
                    .show()
            }
            buttonVisibility()
        }


        buttonStartWork.setOnClickListener { buttonView ->
            if(listView.checkedItemPosition!=-1) {
                selectedID = listView.adapter.getItemId(listView.checkedItemPosition)
                taskStatus = dbhandler.getAFSTasksStatus(selectedID.toInt())

                if (taskStatus == "Traveling") {

                    dbhandler.updateAFSTaskStatus("Working", selectedID.toInt())
                    adapter.changeCursor(dbhandler.getAFSTasksListContents())

                    Toast.makeText(
                        this.baseContext, "Item with ID = "+selectedID.toString()+ " has changed status to 'Working'",
                        Toast.LENGTH_LONG
                    ).show()

                } else {
                    Toast.makeText(
                        this.baseContext, "Unable to set status 'Working'",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }else{Toast.makeText(
                this.baseContext, "Select ITEM no the list",
                Toast.LENGTH_LONG).show()
            }
            buttonVisibility()
        }


        buttonStop.setOnClickListener { buttonView ->
            if(listView.checkedItemPosition!=-1){
                selectedID = listView.adapter.getItemId(listView.checkedItemPosition)
                taskStatus = dbhandler.getAFSTasksStatus(selectedID.toInt())

                if (taskStatus == "Working") {

                    dbhandler.updateAFSTaskStatus("Open", selectedID.toInt())
                    adapter.changeCursor(dbhandler.getAFSTasksListContents())
                    Toast.makeText(
                        this.baseContext, "Item with ID = "+selectedID.toString()+ " has changed status to 'Open'",
                        Toast.LENGTH_LONG
                    ).show()
                }else{
                    Toast.makeText(
                        this.baseContext, "Unable to set status 'Open'",
                        Toast.LENGTH_LONG).show()
                }
            }else{Toast.makeText(
                this.baseContext, "Select ITEM no the list",
                Toast.LENGTH_LONG).show()
            }
            buttonVisibility()
        }

        readJson()

        initData()

        buttonVisibility()

    }

}

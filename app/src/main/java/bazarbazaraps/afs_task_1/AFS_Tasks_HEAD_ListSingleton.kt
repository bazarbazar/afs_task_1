package bazarbazaraps.afs_task_1

class AFS_Tasks_HEAD_ListSingleton {

    var AFS_Task_HEAD_table: ArrayList<AFS_Task_HEAD> =ArrayList<AFS_Task_HEAD>()

    companion object {

        val instance: AFS_Tasks_HEAD_ListSingleton  by lazy{ AFS_Tasks_HEAD_ListSingleton() }
    }

}
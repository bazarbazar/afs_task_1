package bazarbazaraps.afs_task_1

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.ArrayList


class MyDBHandler(context: Context, name: String?,
                      factory: SQLiteDatabase.CursorFactory?, version: Int) :
        SQLiteOpenHelper(context,
            DATABASE_NAME, factory,
            DATABASE_VERSION
        ) {


        companion object {

            private var DATABASE_VERSION = 1
            private var DATABASE_NAME = "AFS_Tasks.db"


            //-----------------------TASK-----------------------------
            var TABLE_AFS_TASKS = "afs_tasks"
            var COLUMN_AFS_TASKS_ID = "_id"
            var COLUMN_AFS_TASKS_NAME = "afs_tasks_name"
            var COLUMN_AFS_TASKS_STATUS = "afs_tasks_status"
        }


        override fun onCreate(db: SQLiteDatabase){
            val CREATE_TABLE_AFS_TASKS = ("CREATE TABLE " +
                    TABLE_AFS_TASKS + "("
                    + COLUMN_AFS_TASKS_ID + " INTEGER PRIMARY KEY," +
                    COLUMN_AFS_TASKS_NAME + " TEXT  unique," +
                    COLUMN_AFS_TASKS_STATUS + " TEXT" +")")

            db.execSQL(CREATE_TABLE_AFS_TASKS)
        }


        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int){

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_AFS_TASKS)

            onCreate(db)
        }


        fun insertAFSTask(afs_task: AFS_Task){
            val values = ContentValues()
            values.put(COLUMN_AFS_TASKS_NAME,afs_task.name)
            values.put(COLUMN_AFS_TASKS_STATUS,afs_task.status)

            val db = this.writableDatabase
            db.insert(TABLE_AFS_TASKS, null, values)

        }


        fun updateAFSTaskStatus(status: String, id:Int){
            val values = ContentValues()
            values.put(COLUMN_AFS_TASKS_STATUS,status)

            val db = this.writableDatabase
            db.update(TABLE_AFS_TASKS,values,  COLUMN_AFS_TASKS_ID + " = " +id ,null)

        }


        fun getAFSTasksListContents(): Cursor {
            val db = this.readableDatabase
            val values = db.rawQuery("SELECT * FROM " + TABLE_AFS_TASKS, null)

            return  values
        }


        fun getAFSTasksListContentsId(id:Int): Cursor {
            val db = this.readableDatabase
            val values = db.rawQuery("SELECT * FROM " + TABLE_AFS_TASKS +" WHERE " + COLUMN_AFS_TASKS_ID + "=" +(id+1) , null)

            return  values
        }


        fun getAFSTasksRowsCount():Int {
            val data = this.getAFSTasksListContents()
            val numRows = data.count

            return  numRows
        }


        fun getAFSTasksStatusCount(status:String):Int {

            val db = this.readableDatabase
            val values = db.rawQuery("SELECT * FROM " + TABLE_AFS_TASKS +" WHERE " + COLUMN_AFS_TASKS_STATUS + "= ?" ,
                arrayOf(status))
            val res =values.count
            values.close()
            db.close()

            return  res
        }


        fun getAFSTasksStatus(id:Int): String {
            val db = this.readableDatabase
            val values = db.rawQuery("SELECT " + COLUMN_AFS_TASKS_STATUS + " FROM " + TABLE_AFS_TASKS +" WHERE " + COLUMN_AFS_TASKS_ID + "= "+id ,
                null)

            val data = values
            data.moveToFirst()

            val res :String
            res = data.getString(0)
            values.close()
            db.close()
            return res
        }


        fun getAFSTasksID(cursor: Cursor):Int{
            val db = this.readableDatabase
            val res :String
            cursor.moveToFirst()
            res = cursor.getString(0)
            return res.toInt()
        }

        fun getColumnNames(): ArrayList<String> {
            val res =arrayListOf<String>(
                COLUMN_AFS_TASKS_ID,
                COLUMN_AFS_TASKS_NAME,
                COLUMN_AFS_TASKS_STATUS
            )

            return res
        }
    }

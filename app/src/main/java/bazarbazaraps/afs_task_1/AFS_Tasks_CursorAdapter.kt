package bazarbazaraps.afs_task_1

import android.content.Context
import android.database.Cursor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.cursoradapter.widget.CursorAdapter


class AFS_Tasks_CursorAdapter(context: Context?, cursor: Cursor?) :
    CursorAdapter(context, cursor, 0) {

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    override fun newView(context: Context?, cursor: Cursor?, parent: ViewGroup?): View {
        val inflater = context
            ?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.row_view_afs_task, parent, false)
        return rowView
    }


    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    override fun bindView(
        view: View,
        context: Context?,
        cursor: Cursor
    ) {
        val ID_View = view.findViewById(R.id.AFS_Task_ID_View) as TextView
        ID_View.text = cursor.getString(cursor.getColumnIndexOrThrow("_id")).toString()

        val nameView = view.findViewById(R.id.AFS_Task_name_View) as TextView
        nameView.text = cursor.getString(cursor.getColumnIndexOrThrow("afs_tasks_name"))

        val statusView = view.findViewById(R.id.AFS_Task_status_View) as TextView
        statusView.text = cursor.getString(cursor.getColumnIndexOrThrow("afs_tasks_status"))

        fun rowColorHighLight(){
            if (statusView.text == "Open"){
                view.setBackgroundColor(ContextCompat.getColor(context!!,R.color.colorLightGreen))
            }else if(statusView.text == "Traveling"){
                view.setBackgroundColor(ContextCompat.getColor(context!!,R.color.colorLightYellow))
            }else if(statusView.text == "Working") {
                view.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorBubbleGum))
            }
        }

        rowColorHighLight()
    }
}
package bazarbazaraps.afs_task_1

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class AFS_Tasks_HEAD_ListAdapter (context: Context?, private val values: ArrayList<AFS_Task_HEAD>) : ArrayAdapter<AFS_Task_HEAD>(context, -1, values) {

    //var mClickListener = listener;

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.row_view_afs_task, parent, false)

        val ID_View = rowView.findViewById<TextView>(R.id.AFS_Task_ID_View) as TextView
        ID_View.text = values[position]._id

        val nameView = rowView.findViewById<TextView>(R.id.AFS_Task_name_View) as TextView
        //textView2.text = values[position].amount.toString()
        nameView.text = values[position].name

        val statusView = rowView.findViewById<TextView>(R.id.AFS_Task_status_View) as TextView
        //textView3.text = values[position].income_cash.multiply(values[position].amount).multiply(values[position].income_cash_mod).toString()
        statusView.text = values[position].status



        return rowView
    }
}